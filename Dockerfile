FROM centos:7

# Install Python 3 and pip
RUN yum -y update && yum install -y python3 python3-pip

# Install Flask and other dependencies
RUN pip3 install flask

# Generate the en_US.utf8 locale
RUN localedef -i en_US -f UTF-8 en_US.UTF-8

# Set the LC_ALL environment variable
ENV LC_ALL en_US.utf8

# Copy your Python application code and test code to /opt/
COPY app.py /opt/
COPY test.py /opt/

# Set the working directory
WORKDIR /opt/

# Define the entry point
CMD ["python3", "app.py"]
