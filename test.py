import os
import unittest
from app import app

class BasicTestCase(unittest.TestCase):

    # Ensure that the base page returns a valid response
    def test_base_page(self):
        self.assertTrue(1 == 1)

if __name__ == '__main__':
    unittest.main()
