import os
import unittest
from flask import Flask, render_template

app = Flask(__name__, template_folder='templateFiles', static_folder='staticFiles')

@app.route("/")
def main():
    return "Welcome!"

@app.route("/shyntas")
def base():
    return render_template('./base.html')

@app.route("/test")
def test():
    return "Test route"

if __name__ == "__main__":
    app.run(debug=True)
